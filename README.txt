
Multipage Controller
====================

This is a simple module intended to make creating multipage forms easier.
The controller lets drupal_get_form do most of the work, and steps in 
after a successful form submission to store the data for the current
page and send the user to the next page.

By default, the controller will take the user through the pages of the
form in the order they are defined in the 'pages' array of the form
data structure. Any page's submit callback can override this behaviour 
by returning a path to another page in the form.

If a page's submit callback returns a path outside of the form, the 
multipage controller will clean up its session store and redirect
out of the form.

It requires consuming modules to create a simple array data structure 
describing the Drupal path to each page in the multipage form, and a 
callback that will build the form array for each page as per the 
Drupal 6 Forms API. Each page's path must be unique.

A hook_menu item(s) that routes requests to drupal_get_form and 
passes 'multipage_controller' and the name of the module-defined callback
as the first and second argument respectively. Any further arguments will
be passed to the module defined callback for the current page.

Example code is included:

<code>
function hook_menu() {
  $items = array();
  /**
   * can be just one base menu item, or can be any arbitrary combination,
   * the only constraint is that each page's path be unique.
   */
  $items['path/to/multipage/form'] = array(
    'title' => 'Multi page test',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('multipage_controller', 'multipage_test_form'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * multipage_test_form 
 */
function multipage_test_form() {

  $form_data = array(

    // The name of the key in $_SESSION Where multipage 
    // module should store information between pages reqeusts.
    'session name' => 'testing',

    // Whether to output a bunch of debug information at the bottom
    // of each page of your form. DON'T LEAVE THIS ON IN A LIVE SITE!!'
    'debug' => TRUE,

    // Whether to allow a page in the form to skip
    // submission processing.
    'simple progression' => TRUE,

    'pages' => array(
      array(
        'path' => 'path/to/multipage/form/1',
        'callback' => <module defined callback>,

        // by default, a submit callback will be built by appending '_submit'
        // to the callback for this page. if you want to override this, set
        // it explicitly here.
        'submit callback' => 'multipage_test_non_standard_submit_handler',
        'title' => 'This is a title',
        'first page' => TRUE,
      ),
      array(
        'path' => 'path/to/multipage/form/2',
        'callback' => <module defined callback>,
        // if this is set to false, a request to this page after a successful
        // submit will of fail, and the user will be sent to the previous page
        'back allowed' => FALSE,

        'title callback' => 'This is a title',
      ),
      array(
        'path' => 'path/to/multipage/form/3',
        'callback' => <module defined callback>,
      ),
      array(
        'path' => 'path/to/multipage/form/3',
        'callback' => <module defined callback>,
      ),
    ),
  );
  return $form_data;
}
</code>

